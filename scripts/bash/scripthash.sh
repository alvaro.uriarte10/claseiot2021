#!/bin/bash

md5sum $1 > $1md5.txt
echo -e "\nEl hash en md5 es: "
cat $1md5.txt

sha1sum $1 > $1sha.txt
echo -e "\nEl hash en sha1 es:"
cat $1sha.txt

sha256sum $1 > $1sha256.txt
echo -e "\nEl hash en sha256 es: "
cat $1sha256.txt

sha512sum $1 > $1sha512.txt
echo -e "\nEl hash en md5 es: "
cat $1sha512.txt
